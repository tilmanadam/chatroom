import {Injectable} from '@angular/core';
import AWSAppSyncClient, {AUTH_TYPE} from 'aws-appsync';
import aws_exports from '../aws-exports';
import {Auth} from 'aws-amplify';


@Injectable({
  providedIn: 'root'
})
export class AppsyncService {
  _hcPublic;

  constructor() {
    console.log(Auth.currentCredentials());
    this._hcPublic = new AWSAppSyncClient({
      url: aws_exports.aws_appsync_graphqlEndpoint,
      region: aws_exports.aws_project_region,
      auth: {
        type: AUTH_TYPE.AWS_IAM,
        credentials: () => Auth.currentCredentials(),
      }
    });
  }

  hcPublic() {
    return this._hcPublic.hydrated();
  }

}
