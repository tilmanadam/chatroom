import {Component} from '@angular/core';
import {AppsyncService} from "./appsync.service";
import gql from 'graphql-tag';
import {listMessages} from "../graphql/queries";
import {createMessage} from "../graphql/mutations";
import {onCreateMessage} from '../graphql/subscriptions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  nameNichtEingegeben = false;
  chatInput: any;
  chatName: any;

  chatmessages: any[];

  constructor(private appsync: AppsyncService) {
    this.appsync.hcPublic().then(client => {
      client.query({
        query: gql(listMessages),
        fetchPolicy: 'network-only'
      }).then(data => {
        console.log(data.data)
        this.chatmessages = data.data.listMessages.items;
        this.chatmessages.sort((a, b) => (a.createdAt > b.createdAt) ? 1 : ((a.createdAt > b.createdAt) ? -1 : 0));
      })

      client.subscribe({
        query: gql(onCreateMessage)
      }).subscribe({
        next: data => {
          this.chatmessages = [...this.chatmessages, data.data.onCreateMessage];
        },
        error: error => {
          console.warn(error);
        }
      })
    })
  }

  sendMessage() {
    if (this.chatName) {
      this.nameNichtEingegeben = false;
      this.appsync.hcPublic().then(client => {
        client.mutate({
          mutation: gql(createMessage),
          variables: {
            input: {
              inhalt: this.chatInput,
              absender: this.chatName
            }
          }
        }).then(result => {
          this.chatInput = '';
          console.log(result);
        })
      });
    } else {
      this.nameNichtEingegeben = true;
    }
  }

}
